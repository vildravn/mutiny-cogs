This is a collection of [Red bot](https://discord.red) cogs developed by the Guild Wars 2 guild _Mutiny on the Jade Sea_.

# Installation

To add this repo to your Red bot, run the following commands, replacing `[p]` with your bot's prefix.

```
[p]load downloader
[p]repo add mutiny-cogs https://gitlab.com/vildravn/mutiny-cogs
```

You may be prompted to respond with "I agree" after that.

# Cogs

## Guild Wars 2

Guild Wars 2 related commands and functions.

### Installation

To install this cog, run the following command, replacing `[p]` with your bot's prefix.

```
[p]cog install mutiny-cogs guildwars2
```

### Commands

| Command     | Description |
| ----------- | ----------- |
| `wiki`      | Posts a Guild Wars 2 wiki link based on the specified search keyword |
| `quaggan`   | Posts a random Quaggan image |

## LFG

A complex Looking for Group system that ties into Discord's Scheduled events and threads.

### Installation

To install this cog, run the following command, replacing `[p]` with your bot's prefix.

```
[p]cog install mutiny-cogs lfg
```

| Command           | Description |
| ----------------- | ----------- |
| `setlfgchannels`  | Sets the text channel that will post LFG threads and voice channel for the Events (Administrator only) |
| `lfg`             | Creates a new LFG thread and scheduled event |

## Mutiny

A collection of commands for Mutiny of the Jade Sea

### Installation

To install this cog, run the following command, replacing `[p]` with your bot's prefix.

```
[p]cog install mutiny-cogs mutiny
```

| Command          | Description |
| ---------------- | ----------- |
| `announce`       | Pings `@everyone` and follows up with a message in a specified channel (Administrator only) |
| `invitemutineer` | Creates an invite that assigns a specified role. Depends on the [RoleInvite](https://laggron.red/roleinvite-api.html) cog (Administrator only) |
| `insult`         | Posts a random pirate themed insult |
| `translate`      | Translates English to Pirate | 

# License

Code in this repository is licensed under the **EUPL-1.2-or-later**.

The [LICENSE](LICENSE) file in this repository is English only, for translations into 23 official EU languages, see [this link](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12).