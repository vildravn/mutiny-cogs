# pylint: disable=missing-module-docstring line-too-long missing-function-docstring
import discord
import pendulum
from redbot.core import Config, app_commands, checks, commands

class RoleDropdown(discord.ui.Select):
    def __init__(self, cog):
        self.cog = cog

        options = [
            discord.SelectOption(label='DPS', description='Pure damage dealer', value='dps', emoji=self.cog.bot.get_emoji(1282085140033572875)),
            discord.SelectOption(label='DPS (Quickness)', description='Offensive support providing Quickness', value='qdps', emoji=self.cog.bot.get_emoji(1282085145792352392)),
            discord.SelectOption(label='DPS (Alacrity)', description='Offensive support providing Alacrity', value='adps', emoji=self.cog.bot.get_emoji(1282085135881211914)),
            discord.SelectOption(label='Healer (Quickness)', description='Defensive support providing Quickness', value='qheal', emoji=self.cog.bot.get_emoji(1282085146870284350)),
            discord.SelectOption(label='Healer (Alacrity)', description='Defensive support providing Alacrity', value='aheal', emoji=self.cog.bot.get_emoji(1282085137781231616)),
            discord.SelectOption(label='Tank', description='Healer that also tanks the boss', value='tank', emoji=self.cog.bot.get_emoji(1282085148518780939)),
        ]

        super().__init__(placeholder='Choose your role(s)...', min_values=1, max_values=len(options), options=options)
    
    async def callback(self, interaction: discord.Interaction):
        thread = interaction.guild.get_thread(interaction.channel.id)
        await self.cog.store_player_role(thread, interaction.user, self.values)
        embed = await self.cog.build_role_embed(thread)
        starter_message = await thread.parent.fetch_message(thread.id)
        await starter_message.edit(embed=embed)

        selected_roles = await self.cog.get_selected_roles(thread, interaction.user)
        await interaction.response.edit_message(content=f'You have chosen the following roles:\n{selected_roles}', view=None)

class RoleDropdownView(discord.ui.View):
    def __init__(self, cog):
        super().__init__()

        self.add_item(RoleDropdown(cog))

class LFG(commands.Cog):
    """A complex Looking for Group system that ties into Discord's Scheduled events and threads."""

    def __init__(self, bot):
        self.bot = bot

        self.config = Config.get_conf(self, identifier=45919)

        self.config.register_guild(
            lfg_channel_id=None,
            event_voice_channel=None,
            event_thread_map=[]
        )

        self.config.register_channel(
            player_roles=[],
            roles_disabled=False
        )

        self.config.register_user(
            saw_role_command_help=False
        )

        self.role_emoji_map = {
            'no_role': '',
            'dps': ' <:dps:1282085140033572875>',
            'qdps': ' <:qdps:1282085145792352392>',
            'adps': ' <:adps:1282085135881211914>',
            'qheal': ' <:qheal:1282085146870284350>',
            'aheal': ' <:aheal:1282085137781231616>',
            'tank': ' <:tank:1282085148518780939>'
        }

        self.role_friendlyname_map = {
            'no_role': 'No role',
            'dps': 'DPS',
            'qdps': 'Quickness DPS',
            'adps': 'Alacrity DPS',
            'qheal': 'Quicknes Heal',
            'aheal': 'Alacrity Heal',
            'tank': 'Tank'
        }

    lfgconfig_commands = app_commands.Group(name="lfgconfig", description="LFG Cog configuration", guild_only=True, default_permissions=discord.Permissions.none())

    @lfgconfig_commands.command()
    @app_commands.default_permissions()
    @commands.guild_only()
    @app_commands.describe(
       lfg_channel='The text channel where LFG threads will be posted',
       party1='One of the three voice channel options for LFG threads/events',
       party2='One of the three voice channel options for LFG threads/events',
       friday_social='One of the three voice channel options for LFG threads/events')
    async def setchannels(self, interaction: discord.Interaction, lfg_channel: discord.TextChannel, party1: discord.VoiceChannel, party2: discord.VoiceChannel, friday_social: discord.VoiceChannel):
        """Sets the LFG related channels"""
        await self.config.guild(interaction.guild).lfg_channel_id.set(lfg_channel.id)
        await self.config.guild(interaction.guild).party1.set(party1.id)
        await self.config.guild(interaction.guild).party2.set(party2.id)
        await self.config.guild(interaction.guild).friday_social.set(friday_social.id)
        await interaction.response.send_message("The LFG channels have been configured", ephemeral=True)

    @lfgconfig_commands.command()
    @app_commands.default_permissions()
    @commands.guild_only()
    async def clearthreadmap(self, interaction: discord.Interaction):
        """A debug command for clearing the thread/event map"""
        await self.config.guild(interaction.guild).event_thread_map.set([])
        await interaction.response.send_message("The event-thread map has been cleared", ephemeral=True)

    @lfgconfig_commands.command()
    @app_commands.default_permissions()
    @commands.guild_only()
    async def inspectthreadmap(self, interaction: discord.Interaction):
        """A debug command that prints out the thread/event map"""
        event_thread_map = await self.config.guild(interaction.guild).event_thread_map()
        await interaction.response.send_message(event_thread_map, ephemeral=True)

    @app_commands.command()
    @app_commands.default_permissions()
    @commands.guild_only()
    async def weeklyevents(self, interaction: discord.Interaction, pvevening: bool = True, raid: bool = True, wvwning: bool = True):
        """Creates LFG threads and scheduled events for regular weekly events"""
        lfg_channel_id = await self.config.guild(interaction.guild).lfg_channel_id()
        party1_channel_id = await self.config.guild(interaction.guild).party1()
        lfg_channel = self.bot.get_channel(lfg_channel_id)
        voice_channel = self.bot.get_channel(party1_channel_id)

        lfg_channel_message = '**A new weekly event has been posted!**\nClick on the \"Interested\" button if you want to join.\nReply in the thread below to discuss the event.'

        if not lfg_channel or not voice_channel:
            await interaction.response.send_message("LFG channels not set", ephemeral=True)
            return

        await interaction.response.defer(ephemeral=True)
        time_now = pendulum.now()

        if pvevening:
            pve_time = time_now.next(pendulum.WEDNESDAY)
            pve_time = pve_time.add(hours=20, minutes=45)
            await self.create_event(
                guild=interaction.guild,
                event_name='Strike Evening',
                event_description='Gather up in a squad and tackle some strikes, or other PvE content depending on the turnout.',
                start_time=pve_time,
                lfg_channel=lfg_channel,
                voice_channel=voice_channel,
                lfg_channel_message=lfg_channel_message)

        if raid:
            raid_time = time_now.next(pendulum.FRIDAY)
            raid_time = raid_time.add(hours=20, minutes=30)
            await self.create_event(
                guild=interaction.guild,
                event_name='Raid Night',
                event_description='Navigate the rough seas that are raids together as a guild. All that is required is good attitude. Check the Yarrpedia for raid guides.',
                start_time=raid_time,
                lfg_channel=lfg_channel,
                voice_channel=voice_channel,
                lfg_channel_message=lfg_channel_message)

        if wvwning:
            wvw_time = time_now.next(pendulum.SUNDAY)
            wvw_time = wvw_time.add(hours=20, minutes=30)
            await self.create_event(
                guild=interaction.guild,
                event_name='WvWning',
                event_description='Join the Mutiny as we battle for control of the Mists. And probably die a lot in the process.',
                start_time=wvw_time,
                lfg_channel=lfg_channel,
                voice_channel=voice_channel,
                lfg_channel_message=lfg_channel_message,
                roles_disabled=True)


        await interaction.followup.send('Events created', ephemeral=True)

    @commands.hybrid_command(name="lfg")
    @commands.guild_only()
    async def lfg(self, ctx: commands.Context):
        """Deprecated command"""
        embed = discord.Embed(
            color=discord.Color.from_str('#D11515'),
            title="",
            description="This command is now deprecated. Please create a [Discord Event](https://support.discord.com/hc/en-us/articles/4409494125719-Scheduled-Events#docs-internal-guid-2f29ff97-7fff-1aeb-a5a9-a92834c856df) instead.")

        await ctx.send(embed=embed, ephemeral=True, delete_after=30.0)
    
    @app_commands.command()
    @commands.guild_only()
    async def role(self, interaction: discord.Interaction):
        """Set your role(s) for this event"""
        thread_event = await self.get_event_thread_pair_thread(interaction.channel)
        await interaction.response.defer(ephemeral=True)
        if thread_event:
            thread = interaction.guild.get_thread(interaction.channel.id)

            roles_disabled = await self.config.channel(thread).roles_disabled()
            if roles_disabled:
                await interaction.followup.send("Role signups are disabled for this event.", ephemeral=True)
                return    

            selected_roles = await self.get_selected_roles(thread, interaction.user)
            view = RoleDropdownView(self)
            await interaction.followup.send(f'What role(s) do you want to play this **{thread.name}**?\n\nCurrently, you have selected:\n{selected_roles}', view=view)
        else:
            await interaction.followup.send("This command only works in event threads.", ephemeral=True)
    
    @app_commands.command()
    @app_commands.default_permissions()
    @commands.guild_only()
    async def toggleroles(self, interaction: discord.Interaction):
        """Enable/Disable role signups for this event"""
        thread_event = await self.get_event_thread_pair_thread(interaction.channel)
        await interaction.response.defer(ephemeral=True)
        if thread_event:
            thread = interaction.guild.get_thread(interaction.channel.id)

            roles_disabled = await self.config.channel(thread).roles_disabled()
            flipped_value = not roles_disabled
            await self.config.channel(thread).roles_disabled.set(flipped_value)
            embed = await self.build_role_embed(thread)
            starter_message = await thread.parent.fetch_message(thread.id)
            await starter_message.edit(embed=embed)
            await interaction.followup.send(f"Role signups are now {'disabled' if flipped_value else 'enabled'}.", ephemeral=True)
        else:
            await interaction.followup.send("This command only works in event threads.", ephemeral=True)

    @commands.Cog.listener()
    async def on_scheduled_event_user_add(self, event: discord.ScheduledEvent, user: discord.User):
        thread_event_pair = await self.get_event_thread_pair(event)
        if thread_event_pair:
            thread = event.guild.get_thread(thread_event_pair["thread"])
            await thread.add_user(user)

            await self.store_player_role(thread, user)
            embed = await self.build_role_embed(thread)
            if embed:
                await self.send_help_command_message(user, thread)
            starter_message = await thread.parent.fetch_message(thread.id)
            await starter_message.edit(embed=embed)
        else:
            lfg_channel_id = await self.config.guild(event.guild).lfg_channel_id()
            lfg_channel = self.bot.get_channel(lfg_channel_id)
            if lfg_channel:
                msg = await lfg_channel.send(f'**<@{event.creator_id}> is looking for a group**\nClick on the \"Interested\" button if you want to join them.\nReply in the thread below to discuss the event.\n{event.url}')
                thread = await msg.create_thread(name=event.name)
                await thread.add_user(user)
                await self.store_event_thread_pair(event.guild, event.id, thread.id)

                await self.store_player_role(thread, user)
                embed = await self.build_role_embed(thread)
                if embed:
                    await self.send_help_command_message(user, thread)
                await msg.edit(embed=embed)

    @commands.Cog.listener()
    async def on_scheduled_event_user_remove(self, event: discord.ScheduledEvent, user: discord.User):
        thread_event_pair = await self.get_event_thread_pair(event)
        if thread_event_pair:
            thread = event.guild.get_thread(thread_event_pair["thread"])
            await thread.remove_user(user)
            await self.remove_player_role(thread, user)

            embed = await self.build_role_embed(thread)
            starter_message = await thread.parent.fetch_message(thread.id)
            await starter_message.edit(embed=embed)

    @commands.Cog.listener()
    async def on_scheduled_event_update(self, before: discord.ScheduledEvent, after: discord.ScheduledEvent):
        thread_event_pair = await self.get_event_thread_pair(after)
        if not thread_event_pair:
            return

        thread = after.guild.get_thread(thread_event_pair["thread"])
        if before.name != after.name:
            await thread.edit(name=after.name)

        if before.start_time != after.start_time:
            await thread.send(f'Heads up {after.guild.default_role}! **The start time has changed!** The event now starts on <t:{int(after.start_time.timestamp())}:F>', allowed_mentions=discord.AllowedMentions(everyone=True))

        if before.status != after.status:
            if after.status == discord.EventStatus.completed:
                await self.archive_event(after, 'This event is now finished.\nThank you for participation!')

    @commands.Cog.listener()
    async def on_scheduled_event_delete(self, event: discord.ScheduledEvent):
        await self.archive_event(event, 'This event has been cancelled.')

    async def create_event(self, guild: discord.Guild, event_name: str, event_description: str, start_time, lfg_channel: discord.TextChannel, voice_channel: discord.VoiceChannel, lfg_channel_message: str, roles_disabled=False):
        event = await guild.create_scheduled_event(
            name=event_name,
            description=event_description,
            entity_type=discord.EntityType.voice,
            channel=voice_channel,
            start_time=start_time,
            privacy_level=discord.PrivacyLevel.guild_only)
        msg = await lfg_channel.send(f'{lfg_channel_message}\n{event.url}')
        thread = await msg.create_thread(name=event.name)
        await self.store_event_thread_pair(guild, event.id, thread.id)

        await self.config.channel(thread).roles_disabled.set(roles_disabled)

        embed = await self.build_role_embed(thread)
        await msg.edit(embed=embed)

    async def archive_event(self, event, message):
        thread_event_pair = await self.get_event_thread_pair(event)
        if thread_event_pair:
            thread = event.guild.get_thread(thread_event_pair["thread"])
            msg = await thread.parent.fetch_message(thread.id)
            await msg.edit(content=message)
            await thread.edit(archived=True)
            await self.remove_event_thread_pair(event)

            await self.config.channel(thread).clear()

    async def store_event_thread_pair(self, guild, event_id, thread_id):
        event_thread_map = await self.config.guild(guild).event_thread_map()
        event_thread = {"event": event_id, "thread": thread_id}
        self.add_pair_to_event_thread_map(event_thread_map, event_thread)
        await self.config.guild(guild).event_thread_map.set(event_thread_map)

    async def remove_event_thread_pair(self, event):
        event_thread_map = await self.config.guild(event.guild).event_thread_map()
        pair = next((item for item in event_thread_map if item["event"] == event.id), None)
        if pair:
            self.remove_pair_from_event_thread_map(event_thread_map, pair)
            await self.config.guild(event.guild).event_thread_map.set(event_thread_map)

    async def get_event_thread_pair(self, event: discord.ScheduledEvent):
        event_thread_map = await self.config.guild(event.guild).event_thread_map()

        return next((item for item in event_thread_map if item["event"] == event.id), None)

    async def get_event_thread_pair_thread(self, thread: discord.Thread):
        event_thread_map = await self.config.guild(thread.guild).event_thread_map()

        return next((item for item in event_thread_map if item["thread"] == thread.id), None)

    def add_pair_to_event_thread_map(self, event_thread_map, pair):
        for i in event_thread_map:
            if i["event"] == pair["event"]:
                return

        event_thread_map.append(pair)

    def remove_pair_from_event_thread_map(self, event_thread_map, pair_to_remove):
        for i,_ in enumerate(event_thread_map):
            if event_thread_map[i]["event"] == pair_to_remove["event"]:
                event_thread_map.pop(i)
                return

    async def store_player_role(self, thread: discord.Thread, member: discord.User, roles = []):
        player_roles = await self.config.channel(thread).player_roles()
        data = {"member": member.id, "roles": roles}

        for i,_ in enumerate(player_roles):
            if player_roles[i]["member"] == data["member"]:
                player_roles[i] = data
                await self.config.channel(thread).player_roles.set(player_roles)
                return
        
        player_roles.append(data)
        await self.config.channel(thread).player_roles.set(player_roles)
    
    async def remove_player_role(self, thread: discord.Thread, member: discord.User):
        player_roles = await self.config.channel(thread).player_roles()

        for i,_ in enumerate(player_roles):
            if player_roles[i]["member"] == member.id:
                player_roles.pop(i)
                await self.config.channel(thread).player_roles.set(player_roles)
                return
    
    async def build_role_embed(self, thread):
        roles_disabled = await self.config.channel(thread).roles_disabled()
        if roles_disabled:
            return None
        
        player_roles = await self.config.channel(thread).player_roles()

        role_count = {
            'no_role': 0,
            'dps': 0,
            'qdps': 0,
            'adps': 0,
            'qheal': 0,
            'aheal': 0,
            'tank': 0
        }
        
        player_list = ""
        player_list2 = ""

        for i in player_roles:
            new_line = f'<@{i["member"]}>'
            if i["roles"] == []: role_count['no_role'] += 1
            for r in i["roles"]:
                role_count[r] += 1
                new_line += self.role_emoji_map[r]
            new_line += "\n"

            # Work around 1024 char limit for embed field
            if len(player_list) + len(new_line) < 1024:
                player_list += new_line
            else:
                player_list2 += new_line
        
        embed = discord.Embed(
            color=discord.Color.from_str('#00ff00'),
            title="",
            description="")
        
        embed.add_field(
            name='__Signed up__',
            value=player_list,
            inline=True)
        
        if player_list2 != "":
            embed.add_field(
                name='__Signed up__',
                value=player_list2,
                inline=True)
        
        embed.add_field(
            name='__Stats__',
            value=f'**No role:** {role_count["no_role"]}\n**DPS:** {role_count["dps"]}\n**Quickness DPS:** {role_count["qdps"]}\n**Alacrity DPS:** {role_count["adps"]}\n**Quickness Heal:** {role_count["qheal"]}\n**Alacrity Heal:** {role_count["aheal"]}\n**Tank:** {role_count["tank"]}',
            inline=True)
        
        return embed
    
    async def send_help_command_message(self, user: discord.User, thread: discord.Thread):
        saw_message = await self.config.user(user).saw_role_command_help()

        if saw_message:
            return
        
        try:
            await user.send(f'You can now use a **`/role`** command inside an event thread, to set your preferred roles for that event.\n\n-# This hint was sent to you because you signed up for <#{thread.id}>. You will not see this message on any future sign-ups.')
            await self.config.user(user).saw_role_command_help.set(True)
        except discord.Forbidden:
            return
    
    async def get_selected_roles(self, thread: discord.Thread, user: discord.User):
        player_roles = await self.config.channel(thread).player_roles()
        user_roles = next((member['roles'] for member in player_roles if member['member'] == user.id), None)

        if user_roles == None or user_roles == []:
            return "No roles"
        
        role_list = ""
        for role in user_roles:
            role_list += f'**{self.role_friendlyname_map[role]}**{self.role_emoji_map[role]}\n'
        
        return role_list

