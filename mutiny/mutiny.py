# pylint: disable=missing-module-docstring line-too-long missing-function-docstring
from typing import Optional

import aiohttp
import discord
from discord.ext import tasks
from redbot.core import Config, app_commands, checks, commands


class Mutiny(commands.Cog):
    """A collection of commands for Mutiny of the Jade Sea"""

    def __init__(self, bot):
        self.bot = bot

        self.config = Config.get_conf(self, identifier=45919)

        self.config.register_guild(
            gw2_guild_id=None,
            gw2_api_key=None,
            log_output_channel=None,
            last_log_id=None
        )

        self.guild_log_checker.start() # pylint: disable=no-member

    @app_commands.command()
    @app_commands.guild_only()
    @app_commands.default_permissions()
    @app_commands.describe(
        channel='The channel to broadcast the message into',
        message='The message that will be broadcasted')
    async def announce(self, interaction: discord.Interaction, channel: discord.TextChannel, message: str):
        """Pings @everyone and follows up with a message in a specified channel"""

        try:
            await channel.send(f'{interaction.guild.default_role}\n{message}', allowed_mentions=discord.AllowedMentions(everyone=True))
        except discord.Forbidden:
            await interaction.response.send_message('Failed to broadcast the message:\nThe bot does not have access to that channel.', ephemeral=True)
        except discord.HTTPException as ex:
            await interaction.response.send_message(f'Failed to broadcast the message:\n{ex.status}: {ex.text}', ephemeral=True)
        else:
            await interaction.response.send_message('Your message has been broadcasted', ephemeral=True)

    @app_commands.command()
    @app_commands.guild_only()
    @app_commands.default_permissions()
    async def invitemutineer(self, interaction: discord.Interaction, role: discord.Role, name: str):
        """Creates an invite that assigns a specified role
      
        You need to specify a name for later tracking purposes"""

        roleinvite = self.bot.get_cog('RoleInvite')

        if not roleinvite:
            await interaction.response.send_message("The RoleInvite cog is not loaded. This command is unavailable", ephemeral=False)
            return

        ri_api = roleinvite.api

        invitelink = await interaction.channel.create_invite(max_age=86400, max_uses=3, reason=name)

        result = await ri_api.add_invite(guild=interaction.guild, invite=invitelink, roles=[role.id])

        if result:
            await interaction.response.send_message(f'Invite link for **{name}** created!\n{invitelink}', ephemeral=False)
        else:
            await interaction.response.send_message("Something went wrong, deleting the invite", ephemeral=True)
            await invitelink.delete()

    @app_commands.command()
    @app_commands.describe(hidden='If True, only you will see the response')
    async def insult(self, interaction: discord.Interaction, hidden: Optional[bool] = False):
        """Posts a random pirate themed insult"""
        api_url = 'https://pirate.monkeyness.com/api/insult'

        async with aiohttp.ClientSession() as client_session:
            async with client_session.get(api_url) as req:
                insult = await req.text()
                await interaction.response.send_message(insult, ephemeral=hidden)

            await client_session.close()

    @app_commands.command()
    @app_commands.describe(text='The text to translate')
    async def translate(self, interaction: discord.Interaction, text: str):
        """Translates English to Pirate"""
        api_url = 'https://pirate.monkeyness.com/api/translate?english='

        async with aiohttp.ClientSession() as client_session:
            async with client_session.get(api_url + text) as req:
                translated = await req.text()
                await interaction.response.send_message(translated, ephemeral=True)

            await client_session.close()


    audit_commands = app_commands.Group(name="auditlog", description="Guild audit log configuration", guild_only=True, default_permissions=discord.Permissions.none())

    @audit_commands.command()
    @app_commands.guild_only()
    @app_commands.default_permissions()
    @app_commands.describe(
       gw2_guild_id='ID of the guild you want to track the audit log of',
       gw2_api_key='The API key used for the calls, needs to have owner access for the guild',
       log_output_channel='Channel where audit log messages will be sent')
    async def config(self, interaction: discord.Interaction, gw2_guild_id: str, gw2_api_key: str, log_output_channel: discord.TextChannel):
        await self.config.guild(interaction.guild).gw2_guild_id.set(gw2_guild_id)
        await self.config.guild(interaction.guild).gw2_api_key.set(gw2_api_key)
        await self.config.guild(interaction.guild).log_output_channel.set(log_output_channel.id)
        await interaction.response.send_message("Guild audit log configured", ephemeral=True)

    @audit_commands.command()
    @app_commands.guild_only()
    @app_commands.default_permissions()
    async def setlastid(self, interaction: discord.Interaction, last_log_id: str):
        await self.config.guild(interaction.guild).last_log_id.set(last_log_id)
        await interaction.response.send_message("Last log ID set.", ephemeral=True)

    @tasks.loop(seconds=600.0)
    async def guild_log_checker(self):
        for guild in self.bot.guilds:
            gw2_guild_id = await self.config.guild_from_id(guild.id).gw2_guild_id()
            gw2_api_key = await self.config.guild_from_id(guild.id).gw2_api_key()
            log_output_channel = await self.config.guild_from_id(guild.id).log_output_channel()

            if gw2_guild_id is None or gw2_api_key is None or log_output_channel is None:
                continue

            api_url = f'https://api.guildwars2.com/v2/guild/{gw2_guild_id}/log?access_token={gw2_api_key}'

            last_log_id = await self.config.guild_from_id(guild.id).last_log_id()

            if not last_log_id is None:
                api_url = api_url + f'&since={last_log_id}'

            async with aiohttp.ClientSession() as client_session:
                async with client_session.get(api_url) as req:
                    events = await req.json()
                    for event in events:
                        event_type = event['type']
                        channel = self.bot.get_channel(log_output_channel)
                        embed = discord.Embed()

                        if event_type == 'invited':
                            user = event['user']
                            invited_by = event['invited_by']
                            embed.color = 3093216
                            embed.description = f'**{user}** was invited into the guild by **{invited_by}**'
                        elif event_type == 'joined':
                            user = event['user']
                            embed.color = 3133024
                            embed.description = f'**{user}** has joined the guild.'
                        elif event_type == 'kick':
                            user = event['user']
                            kicked_by = event['kicked_by']
                            embed.color = 13702421
                            if kicked_by == user:
                                embed.description = f'**{user}** has left the guild'
                            else:
                                embed.description = f'**{user}** was kicked from the guild by **{kicked_by}**'
                        else:
                            continue

                        await channel.send(embed=embed)

                    if len(events) > 0:
                        await self.config.guild_from_id(guild.id).last_log_id.set(events[0]['id'])

                await client_session.close()
