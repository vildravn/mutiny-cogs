class Data:
    daily_ibs_strike = [
        "Cold War",
        "Fraenir of Jormag",
        "Shiverpeaks Pass",
        "Voice of the Fallen and Claw of the Fallen",
        "Whisper of Jormag",
        "Boneskinner",
    ]

    daily_eod_strike = [
        "Aetherblade Hideout",
        "Xunlai Jade Junkyard",
        "Kaineng Overlook",
        "Harvest Temple",
        "Old Lion's Court"
    ]

    daily_soto_strike = [
        "Cosmic Observatory",
        "Temple of Febe"
    ]

    weekly_raid = [
        {"name": "Spirit Vale", "shortcut": "W1"},
        {"name": "Salvation Pass", "shortcut": "W2"},
        {"name": "Stronghold of the Faithful", "shortcut": "W3"},
        {"name": "Bastion of the Penitent", "shortcut": "W4"},
        {"name": "Hall of Chains", "shortcut": "W5"},
        {"name": "Mythwright Gambit", "shortcut": "W6"},
        {"name": "The Key of Ahdashim", "shortcut": "W7"},
        #{"name": "Mount Balrior", "shortcut": "W8"},
    ]

    daily_eod_slayer = [
        "Daily Seitung Province Unchained Slayer",
        "Daily New Kaineng Jade Brotherhood Slayer",
        "Daily Echovald Wilds Jade Mech Slayer",
        "Daily Seitung Province 'Blade Slayer",
        "Daily New Kaineng Purist Slayer",
        "Daily Echovald Wilds Jade Brotherhood Slayer",
        "Daily Seitung Province Naga Slayer",
        "Daily New Kaineng Unchained Slayer",
        "Daily Echovald Wilds Spirit Slayer",
        "Daily Seitung Province Purist Slayer",
        "Daily New Kaineng Naga Slayer",
        "Daily Echovald Wilds Void Slayer",
        "Daily Dragon's End Naga Slayer",
        "Daily Gyala Delve Jade Brotherhood Slayer",
        "Daily Gyala Delve Kappa Slayer",
    ]

    daily_eod_tasks = [
        "Daily New Kaineng City Event Completer",
        "Daily Seitung Province Taskmaster",
        "Daily Echovald Wilds Event Completer",
        "Daily Dragon's End Event Completer",
        "Daily Seitung Province Event Completer",
        "Daily Echovald Wilds Taskmaster",
        "Daily New Kaineng City Event Completer",
        "Daily Echovald Wilds Event Completer",
        "Daily Seitung Province Event Completer",
        "Daily New Kaineng City Taskmaster",
        "Daily Dragon's End Event Completer",
        "Daily Gyala Delve Event Completer",
    ]

    daily_eod_fisher = [
        "Daily Shiverpeaks Fisher",
        "Daily Desert Fisher",
        "Daily End of Dragons Fisher",
        "Daily Heart of Maguuma Fisher",
        "Daily Ascalon Fisher",
        "Daily Orr Fisher",
        "Daily Kryta Fisher",
        "Daily Maguuma Jungle Fisher",
    ]

    daily_eod_gathervista = [
        "Daily End of Dragons Vista Viewer",
        "Daily End of Dragons Forager",
        "Daily End of Dragons Vista Viewer",
        "Daily End of Dragons Miner",
        "Daily End of Dragons Vista Viewer",
        "Daily End of Dragons Lumberer",
    ]

    daily_lws3 = [
        "Daily Bloodstone Fen",
        "Daily Ember Bay",
        "Daily Bitterfrost Frontier",
        "Daily Lake Doric",
        "Daily Draconis Mons",
        "Daily Siren's Landing",
    ]

    daily_lws4 = [
        "Daily Domain of Istan",
        "Daily Sandswept Isles",
        "Daily Domain of Kourna",
        "Daily Jahai Bluffs",
        "Daily Thunderhead Peaks",
        "Daily Dragonfall",
    ]

    daily_ibs = [
        "Daily Grothmar Valley",
        "Daily Bjora Marches",
        "Daily Drizzlewood Coast",
        "Daily Champions",
    ]