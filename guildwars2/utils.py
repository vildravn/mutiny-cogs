# pylint: disable=missing-module-docstring
import urllib.parse
import aiohttp

class Utils:
    """Utilities used in the guildwars2 cog"""

    @staticmethod
    async def call_gw2_api(api_url):
        """Calls Guild Wars 2 API and returns a json object if Ok or None if not Ok"""
        async with aiohttp.ClientSession() as client_session:
            async with client_session.get(api_url) as res:
                if res.ok:
                    return await res.json()

                return None

    @staticmethod
    def create_wiki_link(keyword: str, formatting: str = '', link_text: str = None):
        """Creates a markdown link linking to the GW2 Wiki"""
        if not link_text:
            link_text = keyword

        link_text = f'{formatting}{link_text}{formatting}'
        return f'[{link_text}](https://wiki.guildwars2.com/wiki/{urllib.parse.quote(keyword)})'
