# pylint: disable=missing-module-docstring line-too-long missing-function-docstring invalid-character-zero-width-space
import asyncio
import base64
import struct
import math
import urllib.parse
from random import choice
from typing import Optional
from enum import Enum

import aiohttp
import discord
import pendulum
import numpy
from redbot.core import app_commands, commands, Config
from redbot.core.utils.views import ConfirmView

from .data import Data
from .fractaldata import FractalData
from .utils import Utils

class GW2Profession(Enum):
    # pylint: disable=invalid-name
    Guardian        = (1, '#72C1D9', 'https://wiki.guildwars2.com/images/d/de/Guardian_%28overhead_icon%29.png')
    Warrior         = (2, '#FFD166', 'https://wiki.guildwars2.com/images/0/02/Warrior_%28overhead_icon%29.png')
    Engineer        = (3, '#D09C59', 'https://wiki.guildwars2.com/images/e/e3/Engineer_%28overhead_icon%29.png')
    Ranger          = (4, '#8EDF44', 'https://wiki.guildwars2.com/images/4/47/Ranger_%28overhead_icon%29.png')
    Thief           = (5, '#C08F95', 'https://wiki.guildwars2.com/images/6/6d/Thief_%28overhead_icon%29.png')
    Elementalist    = (6, '#F68A87', 'https://wiki.guildwars2.com/images/2/2f/Elementalist_%28overhead_icon%29.png')
    Mesmer          = (7, '#B679D5', 'https://wiki.guildwars2.com/images/6/6c/Mesmer_%28overhead_icon%29.png')
    Necromancer     = (8, '#52A76F', 'https://wiki.guildwars2.com/images/a/a9/Necromancer_%28overhead_icon%29.png')
    Revenant        = (9, '#D16E5A', 'https://wiki.guildwars2.com/images/f/f0/Revenant_%28overhead_icon%29.png')

    Dragonhunter    = (1, '#72C1D9', 'https://wiki.guildwars2.com/images/9/91/Dragonhunter_%28overhead_icon%29.png')
    Berserker       = (2, '#FFD166', 'https://wiki.guildwars2.com/images/5/5a/Berserker_%28overhead_icon%29.png')
    Scrapper        = (3, '#D09C59', 'https://wiki.guildwars2.com/images/a/a1/Scrapper_%28overhead_icon%29.png')
    Druid           = (4, '#8EDF44', 'https://wiki.guildwars2.com/images/2/2e/Druid_%28overhead_icon%29.png')
    Daredevil       = (5, '#C08F95', 'https://wiki.guildwars2.com/images/d/df/Daredevil_%28overhead_icon%29.png')
    Tempest         = (6, '#F68A87', 'https://wiki.guildwars2.com/images/0/0b/Tempest_%28overhead_icon%29.png')
    Chronomancer    = (7, '#B679D5', 'https://wiki.guildwars2.com/images/2/2d/Chronomancer_%28overhead_icon%29.png')
    Reaper          = (8, '#52A76F', 'https://wiki.guildwars2.com/images/3/39/Reaper_%28overhead_icon%29.png')
    Herald          = (9, '#D16E5A', 'https://wiki.guildwars2.com/images/8/89/Herald_%28overhead_icon%29.png')

    Firebrand       = (1, '#72C1D9', 'https://wiki.guildwars2.com/images/9/94/Firebrand_%28overhead_icon%29.png')
    Spellbreaker    = (2, '#FFD166', 'https://wiki.guildwars2.com/images/f/fc/Spellbreaker_%28overhead_icon%29.png')
    Holosmith       = (3, '#D09C59', 'https://wiki.guildwars2.com/images/f/f3/Holosmith_%28overhead_icon%29.png')
    Soulbeast       = (4, '#8EDF44', 'https://wiki.guildwars2.com/images/3/3a/Soulbeast_%28overhead_icon%29.png')
    Deadeye         = (5, '#C08F95', 'https://wiki.guildwars2.com/images/d/d6/Deadeye_%28overhead_icon%29.png')
    Weaver          = (6, '#F68A87', 'https://wiki.guildwars2.com/images/7/7f/Weaver_%28overhead_icon%29.png')
    Mirage          = (7, '#B679D5', 'https://wiki.guildwars2.com/images/9/98/Mirage_%28overhead_icon%29.png')
    Scourge         = (8, '#52A76F', 'https://wiki.guildwars2.com/images/a/aa/Scourge_%28overhead_icon%29.png')
    Renegade        = (9, '#D16E5A', 'https://wiki.guildwars2.com/images/a/ad/Renegade_%28overhead_icon%29.png')

    Willbender      = (1, '#72C1D9', 'https://wiki.guildwars2.com/images/1/1e/Willbender_%28overhead_icon%29.png')
    Bladesworn      = (2, '#FFD166', 'https://wiki.guildwars2.com/images/6/6e/Bladesworn_%28overhead_icon%29.png')
    Mechanist       = (3, '#D09C59', 'https://wiki.guildwars2.com/images/c/cb/Mechanist_%28overhead_icon%29.png')
    Untamed         = (4, '#8EDF44', 'https://wiki.guildwars2.com/images/6/6d/Untamed_%28overhead_icon%29.png')
    Specter         = (5, '#C08F95', 'https://wiki.guildwars2.com/images/9/9c/Specter_%28overhead_icon%29.png')
    Catalyst        = (6, '#F68A87', 'https://wiki.guildwars2.com/images/3/36/Catalyst_%28overhead_icon%29.png')
    Virtuoso        = (7, '#B679D5', 'https://wiki.guildwars2.com/images/0/04/Virtuoso_%28overhead_icon%29.png')
    Harbinger       = (8, '#52A76F', 'https://wiki.guildwars2.com/images/9/9e/Harbinger_%28overhead_icon%29.png')
    Vindicator      = (9, '#D16E5A', 'https://wiki.guildwars2.com/images/d/d6/Vindicator_%28overhead_icon%29.png')

    def __init__(self, code, color, big_icon):
        self.code = code
        self.color = color
        self.big_icon = big_icon

class CharacterDropdown(discord.ui.Select):
    def __init__(self, characters, api_key):
        self.api_key = api_key
        super().__init__(placeholder='Choose the character to share...', min_values=1, max_values=1, options=characters)

    async def callback(self, interaction: discord.Interaction):
        self.placeholder = "Sharing character, wait please"
        self.disabled = True
        self.view.stop()
        await interaction.response.edit_message(content=f'Sharing **{self.values[0]}.**\nPlease wait, the embed takes a moment to generate... <a:loading:1136361030990176316>', view=None)

        item_slots = [
            'Helm',
            'Shoulders',
            'Coat',
            'Gloves',
            'Leggings',
            'Boots',
            'Backpack',
            'Accessory1',
            'Accessory2',
            'Amulet',
            'Ring1',
            'Ring2',
            'WeaponA1',
            'WeaponA2',
            'WeaponB1',
            'WeaponB2',
        ]

        two_handed = [
            'Greatsword',
            'Hammer',
            'LongBow',
            'Rifle',
            'ShortBow',
            'Staff'
        ]

        character = await self.get_user_character_info(self.api_key, self.values[0])
        if not character:
            await interaction.edit_original_response(content=f'Something went wrong, could not retrieve information about **{self.values[0]}**.')
            return
        name = character['name']
        level = character['level']
        race = character['race']
        profession = character['profession']
        active_build_tab = character['active_build_tab'] - 1
        active_build = character['build_tabs'][active_build_tab]['build']
        active_specs = active_build['specializations']
        active_spec_ids = [active_specs[0]["id"], active_specs[1]["id"], active_specs[2]["id"]]
        active_equip_tab = character['active_equipment_tab']
        all_equip = character['equipment']

        selected_specializations = await self.get_specializations(self.join_int_list(active_spec_ids))

        if selected_specializations:
            for spec_id in selected_specializations:
                if selected_specializations[spec_id].get('elite'):
                    profession = selected_specializations[spec_id].get('name')
                    break

        embed = discord.Embed(
            color=discord.Color.from_str(GW2Profession[profession].color),
            title=name,
            description=f'Level {level} {race} {profession}')
        embed.set_thumbnail(url=GW2Profession[profession].big_icon)

        statlist = {}
        itemlist = []
        equipment = {}
        upgrades = []
        for slot in item_slots:
            item = (next((item for item in all_equip if item.get('slot') == slot and active_equip_tab in item['tabs']), None))
            if item:
                equipment[item.get('slot')] = item
                item_id = item.get('id')
                item_stats = item.get('stats')
                item_upgrades = item.get('upgrades')
                if item_stats:
                    statlist[item_id] = item_stats.get('id')
                else:
                    itemlist.append(item_id)

                if item_upgrades:
                    upgrades.extend(item_upgrades)

        item_stats = await self.get_item_stats(self.join_int_list(itemlist))
        statlist = statlist | item_stats
        stat_names = await self.get_stat_names(self.join_int_list(statlist.values()))

        upgrade_names = await self.get_item_names(self.join_int_list(upgrades))

        equipment_string = ""
        for slot in ['Helm', 'Shoulders', 'Coat', 'Gloves', 'Leggings', 'Boots']:
            item = equipment.get(slot)
            if item:
                item_stat_name = stat_names[statlist[item.get('id')]]
                equipment_string += f"**{item_stat_name} {slot}**\n"

                item_upgrades = item.get('upgrades')
                if item_upgrades:
                    for upgrade in item.get('upgrades'):
                        upgrade_name = upgrade_names[upgrade]
                        upgrade_link = Utils.create_wiki_link(upgrade_name)
                        equipment_string += f' ​ ​ {upgrade_link}\n'
            else:
                equipment_string += "Empty\n"

        embed.add_field(
            name="__Equipment__",
            value=equipment_string,
            inline=True)

        trinket_string = ""
        for slot in ['Backpack', 'Amulet', 'Accessory1', 'Accessory2', 'Ring1', 'Ring2']:
            item = equipment.get(slot)
            if not slot.isalpha():
                slot = slot[:-1]
            if item:
                item_stat_name = stat_names[statlist[item.get('id')]]
                trinket_string += f"**{item_stat_name} {slot}**\n"
            else:
                trinket_string += "Empty\n"

        embed.add_field(
            name="__Trinkets__",
            value=trinket_string,
            inline=True)

        weapon_ids = []
        for slot in ['WeaponA1', 'WeaponA2', 'WeaponB1', 'WeaponB2']:
            item = equipment.get(slot)
            if item:
                weapon_ids.append(item.get('id'))

        if weapon_ids:
            weapon_types = await self.get_item_types(self.join_int_list(weapon_ids))

        weapon_a_string = ""
        for slot in ['WeaponA1', 'WeaponA2']:
            item = equipment.get(slot)
            if item:
                item_stat_name = stat_names[statlist[item.get('id')]]
                weapon_type = weapon_types[item.get('id')]
                weapon_a_string += f"**{item_stat_name} {weapon_type}**\n"

                item_upgrades = item.get('upgrades')
                if item_upgrades:
                    for upgrade in item_upgrades:
                        upgrade_name = upgrade_names[upgrade]
                        upgrade_link = Utils.create_wiki_link(upgrade_name)
                        weapon_a_string += f' ​ ​ {upgrade_link}\n'

                if weapon_type in two_handed:
                    break
            else:
                weapon_a_string += "Empty\n"

        if weapon_a_string != "Empty\nEmpty\n":
            embed.add_field(
                name="__Weapon Set 1__",
                value=weapon_a_string,
                inline=False)

        weapon_b_string = ""
        for slot in ['WeaponB1', 'WeaponB2']:
            item = equipment.get(slot)
            if item:
                item_stat_name = stat_names[statlist[item.get('id')]]
                weapon_type = weapon_types[item.get('id')]
                weapon_b_string += f"**{item_stat_name} {weapon_type}**\n"

                for upgrade in item.get('upgrades'):
                    upgrade_name = upgrade_names[upgrade]
                    upgrade_link = Utils.create_wiki_link(upgrade_name)
                    weapon_b_string += f' ​ ​ {upgrade_link}\n'

                if weapon_type in two_handed:
                    break
            else:
                weapon_b_string += "Empty\n"

        if weapon_b_string != "Empty\nEmpty\n":
            embed.add_field(
                name="__Weapon Set 2__",
                value=weapon_b_string,
                inline=False)

        # Fix if missing traits and specs
        spec_string = ""
        trait_ids = []

        for i, _ in enumerate(selected_specializations):
            trait_ids.extend(active_specs[i]["traits"])

        traits = await self.get_trait_names(self.join_int_list(trait_ids))

        for i, spec_id in enumerate(selected_specializations):
            spec_name = selected_specializations[spec_id].get('name')
            spec_link = Utils.create_wiki_link(spec_name, '**')

            spec_string += f'{spec_link}\n'

            spec_traits = active_specs[i].get('traits')
            for trait_id in spec_traits:
                trait_name = traits[trait_id]
                trait_link = Utils.create_wiki_link(trait_name)
                spec_string += f' ​ ​ {trait_link}\n'

        for i in range(3 - len(selected_specializations)):
            spec_string += 'Empty\n'

        embed.add_field(
            name="__Traits__",
            value=spec_string,
            inline=True)

        skill_ids = [active_build['skills'].get('heal'), *active_build['skills'].get('utilities'), active_build['skills'].get('elite')]
        skills = await self.get_skill_names(self.join_int_list(skill_ids))
        skills_string = ""
        for skill_id in skill_ids:
            skill_name = skills.get(skill_id)
            if skill_name:
                skills_string += f'{Utils.create_wiki_link(skill_name, "**")}\n'

            else:
                skills_string += 'Empty\n'

        embed.add_field(
            name="__Skills__",
            value=skills_string,
            inline=True)

        skills_by_palette = await self.get_profession_skills_by_palette(character['profession'])
        palette_ids = []
        for skill_id in skill_ids:
            palette_ids.extend([x for x,y in skills_by_palette if y == skill_id])

        build_code = self.generate_build_code(GW2Profession[profession].code, selected_specializations, trait_ids, palette_ids)

        embed.add_field(
            name="__Build chat code__",
            value=f'`{build_code}`',
            inline=False)

        await interaction.channel.send(f'<@{interaction.user.id}> is sharing their character', embed=embed)
        await interaction.delete_original_response()

    async def get_user_character_info(self, api_key, character_name):
        api_url = f'https://api.guildwars2.com/v2/characters/{character_name}?v=2022-10-01T00:00:00.000Z&access_token={api_key}'

        return await Utils.call_gw2_api(api_url)

    async def get_specializations(self, spec_ids):
        specs = {}
        api_url = f'https://api.guildwars2.com/v2/specializations?ids={spec_ids}&v=2022-10-01T00:00:00.000Z'

        res = await Utils.call_gw2_api(api_url)
        if res:
            for spec in res:
                specs[spec["id"]] = {"name": spec["name"], "elite": spec["elite"], "major_traits": spec["major_traits"]}

        return specs

    async def get_trait_names(self, trait_ids):
        traits = {}
        api_url = f'https://api.guildwars2.com/v2/traits?ids={trait_ids}&v=2022-10-01T00:00:00.000Z'

        res = await Utils.call_gw2_api(api_url)
        if res:
            for trait in res:
                traits[trait["id"]] = trait["name"]

        return traits

    async def get_skill_names(self, skill_ids):
        skills = {}
        api_url = f'https://api.guildwars2.com/v2/skills?ids={skill_ids}&v=2022-10-01T00:00:00.000Z'

        res = await Utils.call_gw2_api(api_url)
        if res:
            for skill in res:
                skills[skill["id"]] = skill["name"]

        return skills

    async def get_item_names(self, item_ids):
        names = {}
        api_url = f'https://api.guildwars2.com/v2/items?ids={item_ids}&v=2022-10-01T00:00:00.000Z'

        res = await Utils.call_gw2_api(api_url)
        if res:
            for item in res:
                names[item["id"]] = item["name"]

        return names

    async def get_item_stats(self, item_ids):
        stats = {}

        api_url = f'https://api.guildwars2.com/v2/items?ids={item_ids}&v=2022-10-01T00:00:00.000Z'

        res = await Utils.call_gw2_api(api_url)
        if res:
            for item in res:
                infix_upgrade = item.get('details').get('infix_upgrade')
                if infix_upgrade:
                    stats[item["id"]] = item["details"]["infix_upgrade"]["id"]
                else:
                    stats[item["id"]] = 50

        return stats

    async def get_item_types(self, item_ids):
        types = {}
        api_url = f'https://api.guildwars2.com/v2/items?ids={item_ids}&v=2022-10-01T00:00:00.000Z'

        res = await Utils.call_gw2_api(api_url)
        if res:
            for item in res:
                types[item["id"]] = item["details"]["type"]

        return types

    async def get_stat_names(self, stat_ids):
        stats = {}

        api_url = f'https://api.guildwars2.com/v2/itemstats?ids={stat_ids}&v=2022-10-01T00:00:00.000Z'

        res = await Utils.call_gw2_api(api_url)
        if res:
            for stat in res:
                stats[stat["id"]] = stat["name"]
                if stat["name"] == "":
                    stats[stat["id"]] = "Unknown"

        return stats

    async def get_profession_skills_by_palette(self, profession_name):
        api_url = f'https://api.guildwars2.com/v2/professions/{profession_name}?v=2022-10-01T00:00:00.000Z'

        res = await Utils.call_gw2_api(api_url)
        if res:
            return res.get('skills_by_palette')
        else:
            return []

    def generate_build_code(self, profession_code, specs, trait_ids, palette_skill_ids):
        chat_code_bytes = [
            struct.pack('B', 0x0d),
            struct.pack('B', profession_code),
        ]

        trait_choices = numpy.array_split(trait_ids, 3)
        for i, spec in enumerate(specs):
            chat_code_bytes.append(struct.pack('B', spec))
            major_traits = numpy.array_split(specs[spec].get('major_traits'), 3)
            trait_bits = []
            for j in range(3):
                index = list(major_traits[j]).index(trait_choices[i][j])
                trait_bits.append(index + 1)
            trait_bits.append(0)
            trait_bits.reverse()
            chat_code_bytes.append(struct.pack('B', self.create_byte(trait_bits))) #Trait choices go here

        for skill_id in palette_skill_ids:
            chat_code_bytes.append(struct.pack('<H', skill_id))
            chat_code_bytes.append(struct.pack('<H', 0x00)) #Aquatic skill

        chat_code_bytes = b"".join(chat_code_bytes)
        chat_code_encoded = base64.b64encode(chat_code_bytes)
        return f'[&{chat_code_encoded.decode("ascii")}]'

    def create_byte(self, bits):
        byte_value = 0
        for bit in bits:
            byte_value = (byte_value << 2) | bit

        return byte_value

    def join_int_list(self, list_to_join, delim: str = ','):
        return delim.join(map(str, list_to_join))

class CharacterDropdownView(discord.ui.View):
    def __init__(self, characters, api_key):
        super().__init__()

        self.add_item(CharacterDropdown(characters, api_key))

class GuildWars2(commands.Cog):
    """Guild Wars 2 related commands and functions"""

    def __init__(self, bot):
        self.bot = bot

        self.config = Config.get_conf(self, identifier=45919)

        self.config.register_user(
            api_key=None
        )

    @app_commands.command()
    @app_commands.describe(
        keyword='The term to look up on the wiki',
        hidden='If True, only you will see the response')
    async def wiki(self, interaction: discord.Interaction, keyword: str, hidden: Optional[bool] = False):
        """Posts a Guild Wars 2 wiki link based on the specified search keyword"""
        await interaction.response.send_message('https://wiki.guildwars2.com/index.php?search=' + urllib.parse.quote(keyword), ephemeral=hidden)

    @app_commands.command()
    async def quaggan(self, interaction: discord.Interaction):
        """Posts a random quaggan image"""

        api_url = 'https://api.guildwars2.com/v2/quaggans/'

        async with aiohttp.ClientSession() as client_session:
            async with client_session.get(api_url) as req:
                res_json = await req.json()
                quaggan = choice(res_json)

                async with client_session.get(api_url + quaggan) as req:
                    res_json = await req.json()
                    await interaction.response.send_message(res_json['url'])

            await client_session.close()

    daily_commands = app_commands.Group(name="daily", description="Posts about various Guild Wars 2 dailies")

    @daily_commands.command()
    @app_commands.describe(hidden='If True, only you will see the response')
    async def pve(self, interaction: discord.Interaction, hidden: Optional[bool] = True):
        """Posts today's PvE Dailies"""

        formatted_date = pendulum.now().strftime('%-d %b %Y')

        await interaction.response.defer(ephemeral=hidden)

        embed = discord.Embed(
            color=discord.Color.from_str('#613A89'),
            title=f'PvE Dailies for {formatted_date}')
        embed.set_thumbnail(url='https://render.guildwars2.com/file/483E3939D1A7010BDEA2970FB27703CAAD5FBB0F/42684.png')

        embed.add_field(
            name="<:Daily_PvE:1029144727712444439> PvE Dailies",
            value="General PvE dailies were replaced by Wizard's Vault.",
            inline=False
        )

        # EoD Dailies
        eod_slayer = self.get_daily_from_table(Data.daily_eod_slayer)
        eod_fisher = self.get_daily_from_table(Data.daily_eod_fisher)
        eod_tasks = self.get_daily_from_table(Data.daily_eod_tasks)
        eod_gathervista = self.get_daily_from_table(Data.daily_eod_gathervista)
        embed.add_field(
            name="<:EoD_Achi:1145012929595449455> Daily End of Dragons",
            value=f'{eod_slayer}\n{eod_fisher}\n{eod_tasks}\n{eod_gathervista}',
            inline=False)

        # Living World Dailies
        lws3 = self.get_daily_from_table(Data.daily_lws3)
        lws4 = self.get_daily_from_table(Data.daily_lws4)
        ibs = self.get_daily_from_table(Data.daily_ibs)
        embed.add_field(
            name="<:Daily_PvE:1029144727712444439> Daily Living World",
            value=f'<:Season_3:1029509159315591238> {Utils.create_wiki_link(lws3, "**")}\n<:Season_4:1029509160590659584> {Utils.create_wiki_link(lws4, "**")}\n<:IBS_Achi:1145012932007186492> {Utils.create_wiki_link(ibs, "**")}',
            inline=False)

        await interaction.followup.send(embed=embed, ephemeral=hidden)

    @daily_commands.command()
    @app_commands.describe(hidden='If True, only you will see the response')
    async def fractals(self, interaction: discord.Interaction, hidden: Optional[bool] = True):
        """Posts today's Daily and Recommended Fractals"""
        when = pendulum.now()
        if interaction.guild:
            thread_event_pair = await self.get_event_thread_pair(interaction.channel)
            if thread_event_pair:
                event = interaction.channel.guild.get_scheduled_event(thread_event_pair["event"])
                if event:
                    when = event.start_time

        formatted_date = when.strftime('%-d %b %Y')

        await interaction.response.defer(ephemeral=hidden)

        embed = discord.Embed(
            color=discord.Color.from_str('#613A89'),
            title=f'Daily Fractals for {formatted_date}',
            description='_Scale, AR, and Instabilities are shown only for Tier 4 Daily Fractals_')
        embed.set_thumbnail(url='https://render.guildwars2.com/file/4A5834E40CDC6A0C44085B1F697565002D71CD47/1228226.png')

        self.add_fractal_fields(embed, when=when)

        await interaction.followup.send(embed=embed, ephemeral=hidden)

    @daily_commands.command()
    @app_commands.describe(hidden='If True, only you will see the response')
    async def strikes(self, interaction: discord.Interaction, hidden: Optional[bool] = True):
        """Posts today's Priority Strikes"""
        when = pendulum.now()
        if interaction.guild:
            thread_event_pair = await self.get_event_thread_pair(interaction.channel)
            if thread_event_pair:
                event = interaction.channel.guild.get_scheduled_event(thread_event_pair["event"])
                if event:
                    when = event.start_time

        formatted_date = when.strftime('%-d %b %Y')

        await interaction.response.defer(ephemeral=hidden)

        embed = discord.Embed(
            color=discord.Color.from_str('#76AAE6'),
            title=f'Priority Strikes for {formatted_date}')
        embed.set_thumbnail(url='https://render.guildwars2.com/file/C34A20B86C73B0DCDC9401ECD22CE37C36B018A7/2271016.png')

        self.add_strike_fields(embed, when=when)

        await interaction.followup.send(embed=embed, ephemeral=hidden)

    @daily_commands.command()
    @app_commands.describe(hidden='If True, only you will see the response')
    async def raids(self, interaction: discord.Interaction, hidden: Optional[bool] = True):
        """Posts this week's Emboldened and Money Raid Wing"""
        when = pendulum.now()
        if interaction.guild:
            thread_event_pair = await self.get_event_thread_pair(interaction.channel)
            if thread_event_pair:
                event = interaction.channel.guild.get_scheduled_event(thread_event_pair["event"])
                if event:
                    when = event.start_time

        formatted_date = when.strftime('%-d %b %Y')

        await interaction.response.defer(ephemeral=hidden)

        embed = discord.Embed(
            color=discord.Color.from_str('#CB4B2C'),
            title=f'Weekly Raids for {formatted_date}')
        embed.set_thumbnail(url='https://render.guildwars2.com/file/9F5C23543CB8C715B7022635C10AA6D5011E74B3/1302679.png')

        self.add_raid_fields(embed, when=when)

        await interaction.followup.send(embed=embed, ephemeral=hidden)

    @daily_commands.command()
    @app_commands.describe(hidden='If True, only you will see the response')
    async def pvp(self, interaction: discord.Interaction, hidden: Optional[bool] = True):
        """Posts today's PvP Dailies"""
        await interaction.response.send_message('This command is temporarily disabled as ArenaNet has reworked the daily system and has not yet provided new APIs.', ephemeral=True)
        return

        formatted_date = pendulum.now().strftime('%-d %b %Y')

        await interaction.response.defer(ephemeral=hidden)
        dailies = await self.get_dailies()

        embed = discord.Embed(
            color=discord.Color.from_str('#613A89'),
            title=f'PvP Dailies for {formatted_date}')
        embed.set_thumbnail(url='https://render.guildwars2.com/file/FE01AF14D91F52A1EF2B22FE0A552B9EE2E4C3F6/511340.png')
        await self.add_daily_field(embed, '', False, dailies['pvp'])

        await interaction.followup.send(embed=embed, ephemeral=hidden)

    @daily_commands.command()
    @app_commands.describe(hidden='If True, only you will see the response')
    async def wvw(self, interaction: discord.Interaction, hidden: Optional[bool] = True):
        """Posts today's WvW Dailies"""
        await interaction.response.send_message('This command is temporarily disabled as ArenaNet has reworked the daily system and has not yet provided new APIs.', ephemeral=True)
        return

        formatted_date = pendulum.now().strftime('%-d %b %Y')

        await interaction.response.defer(ephemeral=hidden)
        dailies = await self.get_dailies()

        embed = discord.Embed(
            color=discord.Color.from_str('#60473C'),
            title=f'WvW Dailies for {formatted_date}')
        embed.set_thumbnail(url='https://render.guildwars2.com/file/2BBA251A24A2C1A0A305D561580449AF5B55F54F/338457.png')
        await self.add_daily_field(embed, '', True, dailies['wvw'])

        await interaction.followup.send(embed=embed, ephemeral=hidden)

    apikey_commands = app_commands.Group(name="apikey", description="Commands to manage the user's Guild Wars 2 API key")

    @apikey_commands.command()
    async def help(self, interaction: discord.Interaction):
        """Sends a guide on how to create a Guild Wars 2 API key"""
        await interaction.response.send_message(
            """API keys are a way to grant third-party tools and applications partial **read-only** access to your Guild Wars 2 account. Each key is granted a fixed set of permissions that limit what third-party tools can access, and you can delete a key at any time to revoke the permissions granted.
            
            ## Creating an API key
            1. Log in to your ArenaNet account settings: <https://account.arena.net/applications>
            1. Click on the "New Key" button
            1. Enter a name of your choice
            1. Give the API key the permission to access the data about your account by selecting the appropriate checkboxes.
               For this bot, you need to select at least `account`, `characters` and `builds` permissions.
            1. Then, after you click on `Create API Key`, you will be able to copy the API key and paste it in the `/apikey add` command.""",
            ephemeral=True)

    @apikey_commands.command()
    @app_commands.describe(api_key='A valid Guild Wars 2 API key')
    async def add(self, interaction: discord.Interaction, api_key: str):
        """Register your Guild Wars 2 API key with the bot so it can be used for other commands"""

        await interaction.response.defer(ephemeral=True)

        api_url = f'https://api.guildwars2.com/v2/tokeninfo/?v=2022-10-01T00:00:00.000Z&access_token={api_key}'
        async with aiohttp.ClientSession() as client_session:
            async with client_session.get(api_url) as res:
                if not res.ok:
                    await interaction.followup.send('Please enter a valid API key!', ephemeral=True)
                    return

                res_json = await res.json()
                key_name = res_json['name']
                key_permissions = res_json['permissions']
                if 'account' in key_permissions and 'builds' in key_permissions and 'characters' in key_permissions:
                    await self.config.user_from_id(interaction.user.id).api_key.set(api_key)
                    await interaction.followup.send(f'API key **{key_name}** saved!', ephemeral=True)
                    return

                await interaction.followup.send(f'API key **{key_name}** needs to have at least the following permissions: **account**, **builds**, **characters**', ephemeral=True)


    @apikey_commands.command()
    async def delete(self, interaction: discord.Interaction):
        """Delete your Guild Wars 2 API key from the bot"""
        await interaction.response.defer(ephemeral=True)

        view = ConfirmView(interaction.user, disable_buttons=True)
        view.confirm_button.style = discord.ButtonStyle.red
        view.confirm_button.label = "Delete"
        view.dismiss_button.label = "Cancel"
        view.message = await interaction.followup.send('Are you sure you want to delete your API key?', view=view, ephemeral=True)

        await view.wait()

        if view.result:
            await self.config.user_from_id(interaction.user.id).api_key.clear()
            await interaction.followup.send('API key deleted!', ephemeral=True)

    @app_commands.command()
    async def character(self, interaction: discord.Interaction):
        """Share the gear, traits and skills of a selected Guild Wars 2 character"""

        options = []
        followup_message = 'Pick a character to share'
        await interaction.response.defer(ephemeral=True)

        characters = await self.get_user_characters(interaction.user)

        if not characters:
            await interaction.followup.send('⛔ Your Guild Wars 2 API key is invalid or you have no characters.\n\nMake sure to set your API key using the `/apikey add` command.\nIf you need help obtaining an API key, use `/apikey help`')
            return

        for character in characters:
            options.append(discord.SelectOption(label=character))
            if len(options) == 25:
                followup_message += "\nOnly 25 most recently logged-on characters are displayed here.\nIf you don't see your character here, please log onto it in game first."
                break

        api_key = await self.config.user_from_id(interaction.user.id).api_key()
        view = CharacterDropdownView(options, api_key)

        await interaction.followup.send(followup_message, view=view)


    async def get_dailies(self):
        api_url = 'https://api.guildwars2.com/v2/achievements/daily?v=2022-10-01T00:00:00.000Z'
        async with aiohttp.ClientSession() as client_session:
            async with client_session.get(api_url) as req:
                res_json = await req.json()
                return res_json

    async def add_daily_field(self, embed: discord.Embed, title: str, inline: bool, dailies):
        dailies_str = ''

        for daily in dailies:
            if daily['level']['max'] == 80:
                if 'required_access' in daily and daily['required_access']['condition'] == "NoAccess":
                    continue
                daily_name = await self.get_achievement_name(daily['id'])
                dailies_str += f'{daily_name}\n'

        embed.add_field(name=title, value=dailies_str, inline=inline)

    async def get_achievement_name(self, achievement_id):
        api_url = f'https://api.guildwars2.com/v2/achievements/{achievement_id}?v=2022-10-01T00:00:00.000Z'
        async with aiohttp.ClientSession() as client_session:
            async with client_session.get(api_url) as req:
                res_json = await req.json()
                return res_json['name']

    def add_strike_fields(self, embed: discord.Embed, when: pendulum.datetime = pendulum.now()):
        ibs = self.get_daily_from_table(Data.daily_ibs_strike, when=when)
        eod = self.get_daily_from_table(Data.daily_eod_strike, when=when)
        soto = self.get_daily_from_table(Data.daily_soto_strike, when=when)

        embed.add_field(
            name="<:IBS_Achi:1145012932007186492> Icebrood Saga",
            value=f'{ibs}',
            inline=False)

        embed.add_field(
            name="<:EoD_Achi:1145012929595449455> End of Dragons",
            value=f'{eod}',
            inline=False)

        embed.add_field(
            name="<:SotO_Achi:1145013502570926241> Secrets of the Obscure",
            value=f'{soto}',
            inline=False)


    def add_raid_fields(self, embed: discord.Embed, when: pendulum.datetime = pendulum.now()):
        emboldened = self.get_weekly_from_table(Data.weekly_raid, when=when)
        money = self.get_weekly_from_table(Data.weekly_raid, offset=1, when=when)

        emboldened_name = emboldened["name"]
        emboldened_shortcut = emboldened["shortcut"]
        embed.add_field(
            name="<:Emboldened:1134541082986811513> Emboldened Wing",
            value=f'**{emboldened_shortcut}:** {emboldened_name}',
            inline=False)

        money_name = money["name"]
        money_shortcut = money["shortcut"]
        embed.add_field(
            name="<:Call_of_the_Mists:1134541080919019691> Call of the Mists (Money Wing)",
            value=f'**{money_shortcut}:** {money_name}',
            inline=False)

    def add_fractal_fields(self, embed: discord.Embed, when: pendulum.datetime = pendulum.now()):
        index = self.get_day_of_year_index(pendulum.now())
        todays_fractals = self.get_daily_from_table(FractalData.daily_fractals, when=when)
        todays_recs = self.get_daily_from_table(FractalData.recommended_fractals, when=when)

        daily_fractals_str = ''
        for fractal in todays_fractals:
            scale = fractal['scale']
            name = fractal['name']
            agony_resistance = FractalData.agony_resistance[scale - 1]
            hs_link_part = name.replace(' ', '-').lower()
            daily_fractals_str += f'**{scale}** - **[{name}](https://hardstuck.gg/gw2/guides/fractals/{hs_link_part})** ​ <:Agony_Resistance:1026203871250481243> {agony_resistance} ​ ​ ​ ​ \n'
            for instab in FractalData.fractal_instabilities[scale][index]:
                instab_name = FractalData.fractal_instability_names[instab]["name"]
                instab_emoji = FractalData.fractal_instability_names[instab]["emoji"]
                daily_fractals_str += f'​ ​ {instab_emoji} {instab_name}\n'

        rec_fractals_str = ''
        for fractal in todays_recs:
            scale = fractal['scale']
            name = fractal['name']
            agony_resistance = FractalData.agony_resistance[scale - 1]
            hs_link_part = name.replace(' ', '-').lower()
            rec_fractals_str += f'**{scale}** - **[{name}](https://hardstuck.gg/gw2/guides/fractals/{hs_link_part})** ​ <:Agony_Resistance:1026203871250481243> {agony_resistance}\n'

        embed.add_field(name='<:T4_Daily_Fractal:1026233714553929798> Daily T4 Fractals', value=daily_fractals_str, inline=True)
        embed.add_field(name='<:Recommended_Fractal:1026203981254496306> Recommended Fractals', value=rec_fractals_str, inline=True)

    def get_daily_from_table(self, table, when: pendulum.datetime = pendulum.now(), offset: int = 0):
        index = self.get_day_of_year_index(when)

        return table[(index + offset) % len(table)]

    def get_weekly_from_table(self, table, when: pendulum.datetime = pendulum.now(), offset: int = 0):
        seconds_since = (when - pendulum.datetime(2022, 6, 27, 8, 30)).total_seconds()
        full_weeks_since = math.floor(seconds_since / (7*24*60*60))

        return table[(full_weeks_since + offset) % len(table)]

    def get_day_of_year_index(self, date_obj):
        day_of_year = date_obj.timetuple().tm_yday - 1
        year = date_obj.timetuple().tm_year

        if (not self.is_leap_year(year)) and (day_of_year > 58):
            day_of_year += 1

        return day_of_year

    def is_leap_year(self, year):
        if (year % 400 == 0) and (year % 100 == 0):
            return True
        elif (year % 4 == 0) and (year % 100 != 0):
            return True
        else:
            return False

    async def get_user_characters(self, user):
        api_key = await self.config.user_from_id(user.id).api_key()
        api_url = f'https://api.guildwars2.com/v2/characters?v=2022-10-01T00:00:00.000Z&access_token={api_key}'

        if not api_key:
            return None

        async with aiohttp.ClientSession() as client_session:
            async with client_session.get(api_url) as res:
                if res.ok:
                    res_json = await res.json()
                    return res_json
                else:
                    return None

    @commands.Cog.listener()
    async def on_thread_create(self, thread: discord.Thread):
        await asyncio.sleep(1)
        strikes_word_list = ['strike', 'strikes', 'pvevening']
        raids_word_list = ['raid', 'raids']
        fractals_word_list = ['fractals', 'fracs', 'recommended', 'recs']

        thread_event_pair = await self.get_event_thread_pair(thread)
        if not thread_event_pair:
            return

        event = thread.guild.get_scheduled_event(thread_event_pair["event"])
        if not event:
            return

        if any(match in thread.name.lower() for match in strikes_word_list):
            formatted_date = event.start_time.strftime('%-d %b %Y')
            view = ConfirmView(disable_buttons=True)
            view.message = await thread.send(f'It seems like you are assembling a squad for Strike Missions. Post Priority Strikes for <t:{int(event.start_time.timestamp())}:F>?', view=view)

            await view.wait()

            if view.result:
                embed = discord.Embed(
                    color=discord.Color.from_str('#76AAE6'),
                    title=f'Priority Strikes for {formatted_date}')
                embed.set_thumbnail(url='https://render.guildwars2.com/file/C34A20B86C73B0DCDC9401ECD22CE37C36B018A7/2271016.png')
                self.add_strike_fields(embed, event.start_time)
                await thread.send(embed=embed)

            await view.message.delete()

        if any(match in thread.name.lower() for match in raids_word_list):
            formatted_date = event.start_time.strftime('%-d %b %Y')
            view = ConfirmView(disable_buttons=True)
            view.message = await thread.send(f'It seems like you are assembling a squad for Raids. Post weekly Raids for <t:{int(event.start_time.timestamp())}:F>?', view=view)

            await view.wait()

            if view.result:
                embed = discord.Embed(
                    color=discord.Color.from_str('#CB4B2C'),
                    title=f'Weekly Raids for {formatted_date}')
                embed.set_thumbnail(url='https://render.guildwars2.com/file/9F5C23543CB8C715B7022635C10AA6D5011E74B3/1302679.png')
                self.add_raid_fields(embed, event.start_time)
                await thread.send(embed=embed)

            await view.message.delete()

        if any(match in thread.name.lower() for match in fractals_word_list):
            formatted_date = event.start_time.strftime('%-d %b %Y')
            view = ConfirmView(disable_buttons=True)
            view.message = await thread.send(f'It seems like you are gathering a party for Fractals. Post daily Fractals for <t:{int(event.start_time.timestamp())}:F>?', view=view)

            await view.wait()

            if view.result:
                embed = discord.Embed(
                    color=discord.Color.from_str('#613A89'),
                    title=f'Daily Fractals for {formatted_date}',
                    description='_Scale, AR, and Instabilities are shown only for Tier 4 Daily Fractals_')
                embed.set_thumbnail(url='https://render.guildwars2.com/file/4A5834E40CDC6A0C44085B1F697565002D71CD47/1228226.png')
                self.add_fractal_fields(embed, event.start_time)
                await thread.send(embed=embed)

            await view.message.delete()

    async def get_event_thread_pair(self, thread: discord.Thread):
        event_thread_map = await self.bot.get_cog('LFG').config.guild(thread.guild).event_thread_map()

        return next((item for item in event_thread_map if item["thread"] == thread.id), None)
